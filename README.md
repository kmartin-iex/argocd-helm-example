# ArgoCD Kustomize Example


ArgoCD manifest files for the ["App of Apps" Pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/#app-of-apps-pattern).
These applications are platform-centric, with capabilities encompassing a range of cross-cutting concerns between platform and application teams.


## Overview

ArgoCD projects and application use this repository as the source-of-truth using GitOps methodology.
Files in this repository should be used only as config and deployment management.
Application code is kept separate from deployment code in this pattern to keep a more clear audit-trail of changes to each environment.
This also serves to de-couple deployed version from application versions.

For more information, see [ArgoCD Best Practices](https://argo-cd.readthedocs.io/en/stable/user-guide/best_practices/#separating-config-vs-source-code-repositories).

## Getting Started

ArgoCD manifest files for applications / application sets (see more [here](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/)) are found in the `applications` directory, which uses Kustomize.
The directory hierarchy allows additional applications to be added solely through Git, with normal review by platform / security team, before pushing changes to different environments.

## Tools

ArgoCD supports plain JSON / YAML manifest files, Kustomize, Helm, and other configuration managers through the use of plugins.
See more [here](https://argo-cd.readthedocs.io/en/stable/user-guide/application_sources/).


## Helm

This example repository uses Helm charts as the deployment mechanism.
The examples in `charts/` show two applications: `custom-app` and `another-app`.
There is also a library chart that helps build a global ConfigMap: `configs`.

Each application builds its own ConfigMap per environment from application-specific default variables that can be passed in at the `charts/{another-app,custom-app}` level.
Or, we can pass them in as a `customEnv` override at the environment-specific level: `staging-env` and `awesome-prod`.

## How to run locally

### Build Chart Dependencies

Ensure each chart has its dependencies updated:

```shell
for chart in "charts/custom-app" "charts/another-app" "staging-env" "awesome-prod"; do helm dependency update $chart; done
```

### Deploy Charts

If you are running a local instance of Kubernetes (minikube, k3d, microk8s, etc.), you can install each environment.
To use the environment `awesome-prod` with a Helm deployment name of `test-prod` as our example:

```shell
helm upgrade --install test-prod awesome-prod/
```

Check the environment variables for the running pod with kubectl:

```shell
kubectl exec -it test-prod-custom-app -- /bin/sh -c "env"
```

Should see output similar to:

```
KUBERNETES_SERVICE_PORT=443
KUBERNETES_PORT=tcp://10.43.0.1:443
DB_CONNECTION=custom-app-db.production.aws.com
HOSTNAME=test-prod-custom-app
SHLVL=1
HOME=/root
ALT_GREETING=Good morning from Custom App!
TERM=xterm
KUBERNETES_PORT_443_TCP_ADDR=10.43.0.1
CUSTOM_APP_CONFIG=this is a default value specifically for custom-app
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
KUBERNETES_PORT_443_TCP_PORT=443
KUBERNETES_PORT_443_TCP_PROTO=tcp
KUBERNETES_PORT_443_TCP=tcp://10.43.0.1:443
KUBERNETES_SERVICE_PORT_HTTPS=443
ENABLE_TLS=true
PWD=/
KUBERNETES_SERVICE_HOST=10.43.0.1
ZK_CONNECTION=zk1.really-real-prod.aws.com
ENABLE_RISKY=false
```

### Clean Up

To delete the chart from the local environment:

```shell
helm delete test-prod
```